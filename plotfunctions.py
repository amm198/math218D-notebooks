
# coding: utf-8

# In[ ]:


def middleaxes(plot):
    # Given a matplotlib plot, this sets the axes to be centered at the origin
    # Returns the plot
    
    # set the x-spine
    plot.spines['left'].set_position('zero')

    # turn off the right spine/ticks
    plot.spines['right'].set_color('none')
    plot.yaxis.tick_left()

    # set the y-spine
    plot.spines['bottom'].set_position('zero')

    # turn off the top spine/ticks
    plot.spines['top'].set_color('none')
    plot.xaxis.tick_bottom()
    
    return plot

