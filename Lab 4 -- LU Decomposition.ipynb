{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# LU Decomposition\n",
    "\n",
    "As you have seen in class over the last week or so, LU decomposition of matrices is a great way to systematically solve systems of equations. Along the way, you discovered *matrix inverses*, *permutation matrices*, *identity matrices*, and *upper and lower triangular matrices*.\n",
    "\n",
    "Today, we will implement an LU decomposition algorithm and use it to solve systems of equations.\n",
    "\n",
    "> ## Make a copy of this notebook (File menu -> Make a Copy...)\n",
    "\n",
    "## Row Reduction and LU Decomposition\n",
    "\n",
    "In class, you saw that the $U$ part of the LU decomposition of a matrix is just the row-reduced form. You already have code for this! Figuring out the $L$ part is a matter of encoding the steps of the row reduction in matrix form. (There is also pivoting, which makes up the $P$ matrix in $PA = LU$. We will get back to that later.)\n",
    "\n",
    "Find your `rowred(A)` code and paste it into the code box below. You'll be doing this sort of thing quite a lot, so it's worth saving all your routines into a file. If you save into a file called *referencefunctions.py*, you can run commands like `from referencefunctions import rowred`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 1** \n",
    "1. Why does this code already give you the $U$ matrix?<br><br>\n",
    "1. By looking back at your class notes, explain what you need to do in order to generate the $L$ matrix:\n",
    "  * You can initialize $L$ to be either a matrix of zeros or the identity matrix. For later purposes, we will choose to initialize to zeros.<br><br>\n",
    "  * What entries should be added to it? When should they be added? Where in the matrix? Write down how the $L$ matrix changes with each step of the row-reduction.<br><br>\n",
    "  * Since we started with a matrix of zeros, there is one last step needed after the code is done running in order to complete the $L$ matrix. What is it?<br><br>\n",
    "\n",
    "**Question 2** By hand, compute the LU decomposition (without pivoting) of the matrix <br><br>\n",
    "\n",
    "$$\\begin{bmatrix} 1 & 2 & 3 \\\\ 4 & 5 & 6 \\\\ 7 & 8 & 10 \\end{bmatrix}$$\n",
    "\n",
    "#### Some Python Notes\n",
    "\n",
    "* Recall that the `np.zeros((m,n))` command returns an $m \\times n$ matrix full of zeros. To get a matrix full of zeros that has the same shape as a matrix *A*, you can use `np.zeros_like(A)`. This is more efficient than getting the shape of *A* first.<br><br>\n",
    "\n",
    "* You can get a $10\\times 10$ diagonal matrix with all diagonal entries being 8 as follows:\n",
    "```python\n",
    "A=np.zeros((10,10))\n",
    "np.fill_diagonal(A,8.)\n",
    "```\n",
    "  Note the dot after the *8*. What does it do?<br><br>\n",
    "* To return more than one output, you can return a *tuple* object like $(A,B)$. You can then run code like:\n",
    "```python\n",
    "L,U=LUnopivot(A)\n",
    "```\n",
    "\n",
    "  You previously saw this notation when using the command `A.shape`.\n",
    "\n",
    "As always, you should read the documentation for the commands mentioned above before using them.\n",
    "\n",
    "**Question 3** \n",
    "1. Copy your row reduction code into the code box below, and rename the function `LUnopivot(A)`.<br><br>\n",
    "1. Make the necessary changes to your code to transform it into LU decomposition. All you really need to do is create and fill the $L$ matrix!<br><br>\n",
    "1. Test your code in two ways:<br><br>\n",
    "  * By running it on the matrix above and comparing your answer to what you got by hand computation.<br><br>\n",
    "  * By multiplying your $L$ and $U$ matrices. What should you get? Do you?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The Need for Pivoting\n",
    "\n",
    "Consider the following matrix from the last homework: $$\\begin{bmatrix} 10^{-4} & 0 & 10^4 \\\\ 10^4 & 10^{-4} & 0 \\\\ 0 & 10^4 & 1\\end{bmatrix}$$\n",
    "In that lab, you saw that the row-reduction code you wrote makes a mess of a system of equations involving this matrix. That led us to implement pivoting.\n",
    "\n",
    "**Question 4** Compute the LU decomposition of this matrix by hand.\n",
    "\n",
    "**Question 5**  \n",
    "1. Run your `LUnopivot(A)` function on this matrix.<br><br>\n",
    "1. Examine $L$ and $U$. Are they the same as your hand computation above?<br><br>\n",
    "1. Output $LU$. Do you get the original matrix back?\n",
    "\n",
    "Those floating point errors are a pretty serious problem!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Adding Pivoting to the Algorithm\n",
    "\n",
    "The above algorithm takes a matrix $A$ and returns a decomposition $A=LU$. Recall that *pivoting* means that before we use a given row to reduce rows below it, we first swap the row with the largest absolute value in the column with the row we are currently at. To keep track of row swaps, we use a *permutation matrix*. \n",
    "\n",
    "**Question 6** Suppose that a $4\\times 4$ matrix $B$ is derived from a matrix $A$ by first swapping rows 1 and 3, then swapping rows 2 and 3, then swapping rows 3 and 4. \n",
    "\n",
    "1. Write down a permutation matrix $P$ and a matrix equation expressing the relationship between $A$, $B$, and $P$.<br><br>\n",
    "\n",
    "1. Explain how you arrived at your matrix for $P$.\n",
    "\n",
    "**Question 7** Consider the first matrix from the last homework: $$\\begin{bmatrix} 1 & 2 & 3 & -2\\\\ 2 & 4 & 1 &  0\\\\ 3 & 3 & 2 & 5 \\\\ -1 & 6 & 2 & 1\\end{bmatrix}.$$ \n",
    "\n",
    "1. Run `LUnopivot(A)` on it. Why does it fail?<br><br>\n",
    "1. Use MPP to compute the *PA=LU* decomposition of this matrix by hand. You can refer to Question 5 from the last homework if you like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Your `rowredpivot(A)` function does row reduction with pivoting. We will modify this function to compute LU decomposition with pivots. Start by pasting in the `rowredpivot(A)` function into the code box belowlike you did the `rowred(A)` function above. Rename the function `LU(A)`.\n",
    "\n",
    "**Question 8** \n",
    "1. First, make the same modifications to the code that you made above. That is, add the initialization and filling of the $L$ matrix, as well as the changes needed to return both matrices.<br><br>\n",
    "1. Next, we will need to add intializing the permutation matrix $P$. What should it be initially? Add in the code to initialize it. You may want to read the documentation for the `np.eye(n)` command...<br><br>\n",
    "1. Next, we need to make sure we permute all the matrices. $U$ is already permuted due to our previous pivoting code. Add lines to permute the other two matrices.<br><br>\n",
    "1. Lastly, we need to return all three matrices. Modify your code to do this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 9** Test your code out by running it on the matrix given above:\n",
    "1. Check that you get the same result as you got in your hand computation from Question 7.<br><br> \n",
    "1. Output both sides of the equation defining LU decomposition with pivoting and check they are equal.<br><br>\n",
    "> **Important Note:** If you did everything correctly, you won't get exact equality: a number that is zero in the original is a very small decimal in *LU*. This has to do with another problem with floating point numbers: *binary representation*. We won't go into this in much detail, but sufice it to say that this decimal misrepresentation is common. Therefore, we do not check if two matrices are exactly equal. Instead, to check if two matrices are close to within a small tolerance, use `np.allclose(A,B)`.\n",
    "<br><br>\n",
    "1. Note that you do *not* get the original matrix back by multiplying *L* by *U*. What is the relationship between the matrix $LU$ and the original matrix?<br><br>\n",
    "1. How can you use the matrix $P$ to recover the original matrix?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using LU Decomposition to Solve Equations\n",
    "\n",
    "As you have seen in class, one of the most common uses of LU decomposition is in the solution of systems of linear equations. In this section, we will use the code you developed above to see how this works in practice. In the next section, we will apply this to fitting functions to data.\n",
    "\n",
    "First, a reminder of how this works:\n",
    "\n",
    "* Given a system of linear equations, convert it into matrix form $Ax=b$;<br><br>\n",
    "* Decompose $A=LU$ (we'll get back to pivoting on the homework);<br><br>\n",
    "* Then $Ax=b \\Leftrightarrow LUx=b$;<br><br>\n",
    "* Let $y=Ux$. Then $Ly=b$. Solve this equation by forward-substitution to find $y$.<br><br>\n",
    "* Lastly, solve $Ux=y$ by back-substitution to find $x$.<br><br>\n",
    "\n",
    "We have code for the first three steps and the last one. We'll need code for the fourth...\n",
    "\n",
    "### Forward Substitution\n",
    "\n",
    "**Question 10** Write a function `fwdsub(L,v)` that takes an lower-triangular matrix $L$ and a vector $v$ and returns the solution of $Lx=v$. This should be a relatively small modification of your `backsub(U,v)` function. Show a test of your code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question 11** Use your LU decomposition code (use `LUnopivot(A)` this time) and your `backsub(U,v)` and `fwdsub(L,v)` functions to solve the following system of equations:<br><br>\n",
    "$$\\begin{array}\n",
    "4x_1 + 6x_2 - x_3 + 2x_4 & = & -22\\\\\n",
    "-x_1 + 9x_2 + 7x_3 - 6x_4 & = & -26\\\\\n",
    "2x_1 + x_2 + 4x_3 - 2x_4 & = & -20\\\\\n",
    "9x_1 + 6x_2 + 3x_3 - 7x_4 & = & -34\\\\\n",
    "\\end{array}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Fitting Polynomial Curves to Data\n",
    "\n",
    "Suppose that we have $n$ data points from an experiment. Each data point consists of a pair of numbers. We will see that using solutions of linear systems of equations, we can fit an $(n-1)$ degree polynomial to the points.\n",
    "\n",
    "#### Back to High School\n",
    "\n",
    "**Question 12** Suppose you only have two data points: $(1,2)$ and $(4,10)$. \n",
    "1. What is $n$?<br><br>\n",
    "1. In this case, what does an $(n-1)$ degree polynomial's graph look like?<br><br>\n",
    "1. Find the $(n-1)$ degree polynomial that fits this data using knowledge from high school.\n",
    "\n",
    "#### Using Linear Algebra\n",
    "\n",
    "In general, an $n$ degree polynomial can be written: $$p(x)=a_nx^n + a_{n-1}x^{n-1} + \\ldots a_1 x + a_0\\mbox{, where } a_n\\neq 0$$\n",
    "\n",
    "**Question 13** Using the above two data points, we can see that a first degree polynomial that fits them must satisfy the system of equations (make sure you see why!): \n",
    "$$\\begin{array}\n",
    "~a_1\\times1 + a_0 & = & 2 \\\\\n",
    "a_1\\times4 + a_0 & = & 10\\\\\n",
    "\\end{array}$$\n",
    "Solve this system and check you get the same answer as above.\n",
    "\n",
    "**Question 14** Suppose that in addition to the above two data points, we also had $(5,1)$. Write down and solve a system of equations to find a quadratic (second degree) polynomial that fits these data points.\n",
    "\n",
    "**Question 15** Find the best polynomial to fit the following data. You are probably going to want to write the data as a system of linear equations and use your earlier code.\n",
    "\n",
    "$x$ | -3 | -2 | -1 | 0 | 1 | 2 | 3\n",
    "--- | :---: | :---: | :---: |:---: |:---: |:---: |:---: |\n",
    "$y$ | -15.991 | -4.36 | -1.603 | -1 | -1.111 | -2.536 | -9.715\n",
    "\n",
    "**Question 16** Suppose you had the following data points: $(1,2)$, $(2,4.1)$, and $(3,5.9)$. While you could use the ideas we just developed to fit a quadratic function to this data, explain why this is probably misguided. (Hint: you may want to plot these points!)\n",
    "\n",
    "We will return to the idea of fitting curves to data in Lab 5, when we talk about *least squares*. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 0,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
